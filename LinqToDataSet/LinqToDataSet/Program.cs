﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace LinqToDataSet
{
    class Program
    {
        static void Main(string[] args)
        {
            ObjectQuery();
        }
        static TrainingDB.ProductsDataTable CreateProducts()
        {
            SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False");
            SqlDataAdapter adapter = new SqlDataAdapter("select * from Products",conn);

            TrainingDB.ProductsDataTable table = new TrainingDB.ProductsDataTable();
            adapter.Fill(table);
            return table;
        }

        static void ObjectQuery()
        {
            var products = from p in CreateProducts()
                           where p.Price < 30
                           select p;

            foreach (var item in products)
            {
                Console.WriteLine("{0}\t{1}",item.Name_,item.Price);
            }
        }
    }
}
