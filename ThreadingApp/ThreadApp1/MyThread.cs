﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp1
{
    public class MyThread
    {
        public Thread t;
        TikTok tt;

        public MyThread(string name,TikTok ttobj)
        {
            t = new Thread(this.Run);
            this.tt = ttobj;
            t.Name = name;
            t.Start();
        }

        public void Run()
        {
            if(t.Name == "Tik")
            {
                for (int i = 0; i < 5; i++)
                {
                    tt.Tik(true);
                }
                tt.Tik(false);
            }
            else
            {
                for (int i = 0; i < 5; i++)
                {
                    tt.Tok(true);
                }
                tt.Tok(false);
            }

        }

    }
}
