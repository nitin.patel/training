﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp1
{
    public class Printer
    {
        public void Print(object message)
        {

            //acquire a lock
            Monitor.Enter(this);
            try
            {
                Console.Write("*** " + message);
                Thread.Sleep(5000);
                Console.WriteLine(" ***");
            }
            catch (Exception)
            {
                //exception handling code goes here
            }
            finally
            {
                Monitor.Exit(this);
            }
            
        }
    }
}
