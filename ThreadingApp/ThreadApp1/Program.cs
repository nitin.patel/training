﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Main() on thread number {0}",Thread.CurrentThread.ManagedThreadId);

            //Printer a = new Printer('.', 1, "a");
            //Printer b = new Printer('*', 10, "b");

            //Thread t1 = new Thread(new ThreadStart(a.Print));
            //Thread t2 = new Thread(new ThreadStart(b.Print));
            //Console.WriteLine("Priority of t1:{0}",t1.Priority);
            //Console.WriteLine("Priority of t2:{0}", t2.Priority);

            //t1.Start();
            //t2.Start();
            //Console.WriteLine("Main() is waiting for child to finish......");
            //t1.Join();
            //t2.Join();

            //Console.WriteLine("Main() Completed!!!!");

            //Thread t = new Thread(new ThreadStart(Test.DoSomething));
            //t.Start();
            //Thread.Sleep(100);
            //t.Abort();
            ////t.Start();
            //t.Join();
            //Console.WriteLine("Done......");

            //Printer p = new Printer();

            ////used to pass parameters to method
            //ParameterizedThreadStart ps = p.Print;

            //Thread t1 = new Thread(ps);
            //Thread t2 = new Thread(ps);
            //Thread t3 = new Thread(ps);

            //t1.Start("Welcome");
            //t2.Start("to");
            //t3.Start("Tripstack");

            //t1.Join();
            //t2.Join();
            //t3.Join();

            TikTok tt = new TikTok();

            MyThread t1 = new MyThread("Tik", tt);
            MyThread t2 = new MyThread("Tok", tt);

            t1.t.Join();
            t2.t.Join();
            Console.WriteLine("Clock Stopped......");
            

        }
    }
}
