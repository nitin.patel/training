﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp1
{
    public class TikTok
    {
        public void Tik(bool running)
        {
            lock (this)
            {
                if (!running)
                {
                    //notify any waiting threads
                    Monitor.Pulse(this);
                    return;
                }

                Console.WriteLine("Tik ");

                //let Tok() to run
                Monitor.Pulse(this);

                //wait Tok() to complete or notify
                Monitor.Wait(this);
            }
        }
        public void Tok(bool running)
        {
            lock (this)
            {
                if (!running)
                {
                    //notify any waiting threads
                    Monitor.Pulse(this);
                    return;
                }

                Console.WriteLine("Tok ");

                //let Tok() to run
                Monitor.Pulse(this);

                //wait Tok() to complete or notify
                Monitor.Wait(this);
            }
        }
    }
}
