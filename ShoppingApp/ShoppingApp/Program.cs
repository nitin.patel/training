﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Runtime.Remoting.Contexts;
using System.Data.Entity;

namespace ShoppingApp
{
    class Program
    {
        static ShoppingDBEntities db = new ShoppingDBEntities();
        
        static void Main(string[] args)
        {
            int choice=0;
            bool flag = true;
            
            while(flag)
            {
                string temp_String = "\nEnter your choice\n" +
                    "1. See the list of Products\n" +
                    "2. See the list of Products Categorywise\n" +
                    "3. Add Product to the Cart\n";
                var cartList = CartClass.GetCart();
                if (cartList.Any())
                {
                    temp_String = temp_String +
                        "4. See Cart Details\n" +
                        "5. Add quantity of Existing Product in Cart\n" +
                        "6. Remove product from Cart\n" +
                        "7. See Summary of the Cart\n";
                }
                Console.Clear();
                Console.WriteLine(temp_String);
                try
                {
                    choice = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception)
                {
                }
                switch (choice)
                {
                    case 1:
                        ListProducts();                     //Function to show all the Products
                        break;
                    case 2:
                        ListCategorywiseProducts();         //Function to show Products Categorywise
                        break;
                    case 3:
                        AddProductToCart();                 //Function to add Products in the Cart
                        break;
                    case 4:
                        SeeCartDetails();                   //Function to show Cart Details
                        break;
                    case 5:
                        AddQuantityOfExistingProduct();     //Function to add quantity to already existing Product
                        break;
                    case 6:
                        RemoveProductFromCart();            //Function to remove product from Cart
                        break;
                    case 7: 
                        SummaryOfCart();                    //Function to see summary of the cart
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Invalid Choice!!!!");
                        break;
                }
                Console.Write("\nEnter 'N' to stop the application or Enter any key to continue:");
                if (Console.ReadLine() == "N")
                {
                    flag = false;
                }
            }
            var cart = CartClass.GetCart();
            foreach (var item in cart)
            {
                db.Carts.Remove(item);
            }
            db.SaveChanges();
        }

        //Function to show summary of Cart
        private static void SummaryOfCart()
        {
            Console.Write("\n");
            int totalItems = 0;
            double price = 0.0;
            var cartList = CartClass.GetCart();
            foreach (var item in cartList)
            {
                totalItems += item.Quantity;
                price += Convert.ToDouble(item.SubTotal);
            }
            Console.WriteLine("Summary of the Cart is given below:");
            Console.WriteLine("Quantity of Items :{0}",totalItems);
            Console.WriteLine("Total Cart Price :{0}",price);
        }

        //Function to remove product from Cart
        private static void RemoveProductFromCart()
        {
            Console.Write("\n");
            SeeCartDetails();
            Console.WriteLine("\nEnter the Name of the Product to be removed from cart");
            string name = Console.ReadLine();
            var cartListTemp = CartClass.GetCart();
            var cartList = CartClass.GetCart();
            foreach (var item in cartListTemp)
            {
                if (item.Name == name)
                {
                    cartList.Remove(item);
                }
            }
            db.SaveChanges();
            Console.WriteLine("Product removed from the cart");
        }

        //Function to add quantity to already existing Product
        private static void AddQuantityOfExistingProduct()
        {
            Console.Write("\nProducts in the cart are follows:");
            SeeCartDetails();
            Console.WriteLine("\nEnter the Id of the Product whose quantity you want to change");
            int id = Convert.ToInt32(Console.ReadLine());
            var cartList = CartClass.GetCart();
            foreach (var item in cartList)
            {
                if (item.Id == id)
                {
                    Console.WriteLine("\nEnter the updated quantity of that product :");
                    int quantity = Convert.ToInt32(Console.ReadLine());
                    item.Quantity = quantity;
                    item.SubTotal = quantity * item.Price;
                    break;
                }
            }
            db.SaveChanges();
            Console.WriteLine("Quantity Changed");
        }

        //Function to show Cart Details
        private static void SeeCartDetails()
        {
            Console.Write("\nCart Details are :");
            var cartList = CartClass.GetCart();
            if (cartList.Any())
            {
                foreach (var item in cartList)
                {
                    Console.WriteLine("Name :{0,-12}\t" +
                        "Quantity :{1,-12}\t" +
                        "Price :{2,-12}\t" +
                        "SubTotal :{3,-12}", item.Name, item.Quantity, item.Price, item.SubTotal);
                }
            }
            else
            {
                Console.WriteLine("No Items in the Cart");
            }
        }

        //Function to add products in the cart
        private static void AddProductToCart()
        {
            Console.Write("\n");
            ListProducts();
            var cartList = CartClass.GetCart();
            Console.WriteLine("\nEnter the Id of the Product you want to add");
            int id = Convert.ToInt32(Console.ReadLine());
            foreach (var item in cartList)
            {
                if(item.Id == id)
                {
                    Console.WriteLine("Product already in the cart");
                    break;
                }
            }
            Product p = new Product();
            var selectedProduct = p;
            try
            {
                selectedProduct = db.Products.Where(j => j.Id == id).Single();
                Cart c = new Cart();
                c.Id = selectedProduct.Id;
                c.Name = selectedProduct.Name_;
                c.Quantity = 1;
                c.Price = selectedProduct.Price;
                c.SubTotal = selectedProduct.Price;
                db.Carts.Add(c);
                db.SaveChanges();
                Console.WriteLine("Product Added Successfully to the Cart");
            }
            catch (Exception)
            {
                Console.WriteLine("Product with Id you entered does'nt exist..........");
            }
        }

        //Function to show Products Categorywise
        private static void ListCategorywiseProducts()
        {
            Console.Write("\n");
            string category = "";
            Console.WriteLine("Available Categories:\n\nSoccer\nBadminton");
            Console.WriteLine("\nEnter the Category whose details you want to see");
            category = Console.ReadLine();
            var list = db.Products.Where(p => p.Category == category);
            foreach (var item in list)
            {
                Console.WriteLine("ID :{0}\t" +
                    "Name :{1,-12}\t" +
                    "Description :{2,-12}\t" +
                    "Category :{3,-12}\t" +
                    "Price :{4,-12}", item.Id, item.Name_, item.Description,item.Category, item.Price);
            }
        }

        // Function to show all the products
        private static void ListProducts()
        {
            var list = db.Products;
            Console.WriteLine("\nList of Products is given below:");
            foreach (var item in list)
            {
                Console.WriteLine("ID :{0}\t" +
                    "Name :{1,-12}\t" +
                    "Description :{2,-12}\t" +
                    "Category :{3,-12}\t" +
                    "Price :{4,-12}", item.Id, item.Name_, item.Description, item.Category, item.Price);
            }
        }
    }
}
