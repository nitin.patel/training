﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingApp
{
    public class CartClass
    {
        static ShoppingDBEntities dbc = new ShoppingDBEntities();

        public static DbSet<Cart> GetCart()
        {
            return dbc.Carts;
        }
    }
}
