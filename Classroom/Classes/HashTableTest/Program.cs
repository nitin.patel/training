﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Text.RegularExpressions;

namespace HashTableTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable hashtable = CollectWords();

            Display(hashtable);
            Console.WriteLine();
        }

        private static void Display(Hashtable hashtable)
        {
            Console.WriteLine("\nHashTable contains:\n{0,-12}{1,-12}","Key:","Value:");
            foreach (var key in hashtable.Keys)
            {
                Console.WriteLine("{0,-12}{1,-12}",key,hashtable[key]);
            }

            Console.WriteLine("\nSize:{0}",hashtable.Count);
        }

        private static Hashtable CollectWords()
        {
            Hashtable table = new Hashtable();
            Console.WriteLine("Enter a string: ");
            string input = Console.ReadLine();

            string[] words = Regex.Split(input, @"\s+");

            foreach (var word in words)
            {
                string wordKey = word.ToLower();
                if (table.ContainsKey(wordKey))
                {
                    table[wordKey] = ((int)table[wordKey]) + 1;
                }
                else
                {
                    table.Add(wordKey, 1);
                }
            }
            return table;
        }
    }
}
