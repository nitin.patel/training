﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace StackTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack stack = new Stack();
            bool aBoolean = true;
            char aChar = '$';
            int anInt = 34567;
            string aString = "Hello";

            //use Push()
            stack.Push(aBoolean);
            PrintStack(stack);
            stack.Push(aChar);
            PrintStack(stack);
            stack.Push(aString);
            PrintStack(stack);
            stack.Push(anInt);
            PrintStack(stack);

            Console.WriteLine("First item in stack is :{0}\n",stack.Peek());
            PrintStack(stack);

            object o = stack.Pop();
            PrintStack(stack);
        }

        private static void PrintStack(Stack stack)
        {
            if(stack.Count == 0)
            {
                Console.WriteLine("Stack is empty\n");
            }
            else
            {
                Console.WriteLine("Stack is :");
                foreach (var item in stack)
                {
                    Console.Write("{0} ",item);
                }
                Console.WriteLine();
            }
        }
    }
}
