﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    public class MyArray
    {
        public MyArray()
        {

        }
        public void Nonstatic()
        {

        }
        public static void StaticMethod()
        {

        }
    }

    public struct Point
    {
        int x, y;
        public Point(int a , int b)
        {
            x = a;
            y = b;
        }
        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
            }
        }
    }
}
