﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateApp
{
    delegate void Compute(int a);

    class Program
    {
        static void Main(string[] args)
        {
            //Compute c = new Compute(Square);
            Compute c = Square;
            c += Cube;
            c.Invoke(13);
              
        }

        static void Square(int x)
        {
            Console.WriteLine("Square:{0}", (x * x));
        }

        static void Cube(int y)
        {
            Console.WriteLine("Cube:{0}", (y * y * y));
        }
    }
}
