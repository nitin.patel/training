﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateApp2
{

    delegate void ClickHandler();

    class Program
    {
        static void Main(string[] args)
        {
            MyButton button = new MyButton();
            button.Click += delegate ()
            {
                Console.WriteLine("MyButton Clicked!!");
            };

            button.RaiseEvent();
        }
    }

    class MyButton
    {
        public event ClickHandler Click;

        public void RaiseEvent()
        {
            Click();
        }
    }
}
