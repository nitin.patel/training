﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StreamReadWriteApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //create file with enable appending "true"
            StreamWriter writer = new StreamWriter(@"c:\training\data.txt", true);

            writer.WriteLine("This is line 1");
            writer.WriteLine("This is line 2");
            writer.WriteLine("This is line 3");

            writer.Close();
            Console.WriteLine("Press a key to read it back");
            Console.ReadLine();

            StreamReader reader = new StreamReader(@"c:\training\data.txt");

            string line="";
            while ((line = reader.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }
            reader.Close();
        }
    }
}
