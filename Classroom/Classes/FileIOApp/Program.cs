﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileIOApp
{
    class Program
    {
        static void Main(string[] args)
        {
            DirectoryInfo info = new DirectoryInfo(@"c:\Windows");       //can also write as "c:\\Winodws"
            Console.WriteLine("Listing contents of {0} Directory: ",info.FullName);
            foreach ( DirectoryInfo file in info.GetDirectories())
            {
                //listing details of every file
                Console.WriteLine("Directory Name: " + file.Name);
                foreach (var f in file.GetFiles())
                {
                    Console.WriteLine("\t" + f.Name);
                }
                
                Console.WriteLine();
            }
        }
    }
}
