﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BinaryWriterApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Open or create a file
            FileStream fs = File.OpenWrite(@"c:\training\product.txt");
            BinaryWriter writer = new BinaryWriter(fs);
            
            //Product Id as integer
            writer.Write(123);
           

            //Product Name  as string
            writer.Write("Product 1");

            //Product Price as double
            writer.Write(34.55);

            //close writer stream(unless and until u dont close stream file doesnt get saved)
            writer.Close();
            fs.Close();

            Console.WriteLine("Product Details are written to file!");

            
            using (FileStream file = File.OpenRead(@"c:\training\product.txt"))
            {
                BinaryReader reader = new BinaryReader(file);

                Console.WriteLine("Product Id: {0}", reader.ReadInt32());
                Console.WriteLine("Product Name: {0}", reader.ReadString());
                Console.WriteLine("Product Price: {0}", reader.ReadDouble());
                reader.Close();
                file.Close();
            }
            //when control gets out of using block automatically dispose method is called for file
            
            
        }
    }
}
