﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ArrayListTest
{
    class Program
    {
        private static readonly string[] colors = { "MAGENTA", "RED", "WHITE", "BLUE", "CYAN" };
        private static readonly string[] removecolors = { "RED", "WHITE", "BLUE" };

        static void Main(string[] args)
        {
            ArrayList list = new ArrayList(1);
            //add elements from static array into this list
            Console.WriteLine("Base Capacity :{0}", list.Capacity);
            foreach (var color in colors)
            {
                list.Add(color);
            }
            Console.WriteLine("Capacity :{0}",list.Capacity);

            ArrayList removeList = new ArrayList(removecolors);
            DisplayInfo(list);

            //remove colors
            removeColors(list,removeList);
            Console.WriteLine("\nArrayList after removing colors: ");
            DisplayInfo(list);

        }

        private static void removeColors(ArrayList list, ArrayList removeList)
        {
            for (int count = 0; count < removeList.Count; count++)
            {
                list.Remove(removeList[count]);
            }
        }

        private static void DisplayInfo(ArrayList list)
        {
            //iterate through list 
            foreach (var item in list)
            {
                Console.Write("{0} ",item);
            }

            Console.WriteLine("\n Size :{0} ; Capacity :{1}", list.Count, list.Capacity);
            list.Add("BLACK");
            list.Add("GREEN");
            list.Add("ORANGE");
            list.Add("YELLOW");
            Console.WriteLine("\n Size :{0} ; Capacity :{1}", list.Count, list.Capacity);
            int index = list.IndexOf("BLUE");
            if (index != -1)
            {
                Console.WriteLine("The ArrayList contains BLUE at index :{0}",index);
            }
            else
            {
                Console.WriteLine("ArrayList doesnt contain BLUE");
            }
        }
    }
}
