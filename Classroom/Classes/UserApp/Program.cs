﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserApp
{
    //public delegate void LoginDelegate(User user);

    class Program
    {
        
        static void Main(string[] args)
        {
            User u1 = new User();
            User u2 = new User();
            u1.UserName = "flight";
            u1.Password = "flight";
            u2.UserName = "nitin";
            u2.Password = "nitin";
            
            var result1 = Login.Authenticate(u1);
            
            var result2 = Login.Authenticate(u2);

            Login l = new Login();
            l.LoginSuccessfull += OnSuccess;
            l.LoginFailed += OnFail;
            l.loginresult(result1,u1);
            l.loginresult(result2,u2);
            
            
        }

        private static void OnFail(LoginEventArgs login)
        {
            Console.WriteLine("User with username: {0} login succeeded",login.UserName);
        }

        private static void OnSuccess(LoginEventArgs login)
        {
            Console.WriteLine("User with username: {0} login failed", login.UserName);
        }
        //public  void loginresult(bool result)
        //{
        //    if (result == true)
        //    {
        //        l = OnSuccess;
        //    }
        //    else
        //    {
        //        l = OnFail;
        //    }
        //}


    }
}
