﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;   

namespace Collections
{
    class Program
    {
        private static int[] intValues = { 1, 2, 3, 4, 5, 6 };
        private static double[] doubleValues = { 8.4, 9.3, 0.2, 7.9, 3.4 };
        private static int[] intValuesCopy;

        static void Main(string[] args)
        {
            //1. Array
            intValuesCopy = new int[intValues.Length];
            Console.WriteLine("Initial Array Values :\n");
            PrintArrays();

            //sort doubleValues
            Array.Sort(doubleValues);

            //copy values from intValues to intValuesCopy array
            Array.Copy(intValues, intValuesCopy, intValues.Length);

            Console.WriteLine("\n Array values after sort and copy :\n");
            PrintArrays();

            //search 5 in intValues array
            int result = Array.BinarySearch(intValues, 5);
            if(result>=0)
            {
                Console.WriteLine("5 Found at element:{0}",result);
            }
            else
            {
                Console.WriteLine("5 Not Found ");
            }
            //search 8783 in intValues
            result = Array.BinarySearch(intValues, 8783);
            if (result>=0)
            {
                Console.WriteLine("8783 Found at :{0}",result);
            }
            else
            {
                Console.WriteLine("8783 Not Found");
            }
        }

        private static void PrintArrays()
        {
            Console.WriteLine("doubleValues: ");
            IEnumerator enumerator = doubleValues.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current + " ");
            }
            Console.WriteLine("\nintValues: ");
            enumerator = intValues.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current + "");
            }
            Console.WriteLine("\nintValuesCopy: ");
            foreach (var element in intValuesCopy)
            {
                Console.WriteLine(element + " ");
            }


        }
    }
}
