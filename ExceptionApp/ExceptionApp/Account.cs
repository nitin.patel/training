﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionApp
{
    public class Account
    {
        private int balance;
        public Account(int amount)
        {
            this.balance = amount;
        }

        public void Withdraw(int amt)
        {
            if (this.balance < amt)
            {
                //throw InsufficientBalanceException
                throw new InsufficientBalanceException("NOT ENOUGH BALANCE!!!!");
            }
            this.balance -= amt;
        }
        public void Deposit(int amt)
        {
            this.balance += amt;
        }
        public int Balance
        {
            get
            {
                return this.balance;
            }
        }

    }
}
