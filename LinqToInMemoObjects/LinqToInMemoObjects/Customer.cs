﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToInMemoObjects
{
    public class Customer
    {
        public string CustomerId { get; set; }
        public string City { get; set; }

        public override string ToString()
        {
            return CustomerId + "\t" + City;
        }
    }
}
