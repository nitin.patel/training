﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LinqToInMemoObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            //NumQuery();
            //ObjectQuery();
            XmlQuery();
            
        }
        static void NumQuery()
        {
            var numbers = new int[] { 1, 4, 9, 6, 25 };

            var evenNumbers = from p in numbers
                              where (p % 2) == 0
                              select p;
            Console.WriteLine("Result: ");
            foreach (var item in evenNumbers)
            {
                Console.WriteLine(item);
            }
        }

        static IEnumerable<Customer> CreateCustomers()
        {
            //return new List<Customer>
            //{
            //    new Customer{ CustomerId = "1",City = "Los Angeles"},
            //    new Customer{ CustomerId = "2",City = "Sydney"},
            //    new Customer{ CustomerId = "3",City = "Kuala Lumpur"},
            //    new Customer{ CustomerId = "4",City = "Georgia"},
            //    new Customer{ CustomerId = "5",City = "Georgia"},
            //    new Customer{ CustomerId = "6",City = "Georgia"}
            //};

            return from c in XDocument.Load("Customers.xml").Descendants("Cusromers").Descendants()
                   select new Customer { City = c.Attribute("City").Value,
                       CustomerId = c.Attribute("CustomerId").Value};
        }

        static void ObjectQuery()
        {
            var objects = from c in CreateCustomers()
                          where c.City == "Georgia"
                          select c;

            foreach (var item in objects)
            {
                Console.WriteLine(item);
            }
        }

        static void XmlQuery()
        {
            var doc = XDocument.Load("Customers.xml");
            var result = from c in doc.Descendants("Customer")
                         where c.Attribute("City").Value == "London"
                         select c;

            XElement transformDoc = new XElement("Londoners",
                                    from customer in result
                                    select new XElement("Contact",
                                    new XAttribute("Id", customer.Attribute("CustomerId").Value)));

            XElement products = new XElement("Products",
                                    new XElement("Product",
                                        new XAttribute("Id", 1),
                                        new XElement("Name", "Foot ball"),
                                        new XElement("Price", 130)),
                                    new XElement("Product",
                                        new XAttribute("Id", 2),
                                        new XElement("Name", "Tennis Ball"),
                                        new XElement("Price", 120))); 

            Console.WriteLine("Result:\n{0} ",products);

            products.Save("Products.xml");
            Console.WriteLine("File Saved.....");
            
        }

        
    }
}
