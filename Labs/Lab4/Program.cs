﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Employee e1 = new Employee(10001, "Ram", "Employee", 10000, 1000);
            //Employee e2 = new Employee(10002, "Shyam", "Manager", 10000, 2000);
            

            Employee e = new Employee();
            List<Employee> emp = new List<Employee>();
            while(true)
            {
                Console.WriteLine("Enter your choice :");
                Console.WriteLine("1. Add a new Employee and Insert its details");
                Console.WriteLine("2. Display all Employee details");
                //Console.WriteLine("3. Display all Department details");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Console.WriteLine("Enter the details of the Employee :");
                        e.EmployeeId = Employee.GenerateId();
                        Console.WriteLine("Name :");
                        e.EmployeeName = Console.ReadLine();
                        Console.WriteLine("Designation :");
                        e.Designation = Console.ReadLine();
                        Console.WriteLine("Basic Salary :");
                        e.BasicSalary = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("DepartmentId :");
                        e.DepartmentId = Convert.ToInt32(Console.ReadLine());
                        emp.Add(e);
                        Console.WriteLine("Enter 'N' to stop the Application:");
                        if(Console.ReadLine() == "N")
                        {
                            break;
                        }
                        break;
                    case 2:
                        if(emp.Count == 0)
                        {
                            Console.WriteLine("No employee entries");
                        }
                        for (int i = 0; i < emp.Count; i++)
                        {
                            Console.WriteLine("Employee" + i + 1 + "Details :");
                            emp[i].display();
                        }
                        Console.WriteLine("Enter 'N' to stop :");
                        if (Console.ReadLine() == "N")
                        {
                            break;
                        }
                        break;
                    default:
                        break;
                }

            }
        }
    }
}
