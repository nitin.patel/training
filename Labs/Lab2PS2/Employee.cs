﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2PS2
{
    class Employee
    {
        private double Hra;
        private double Da;
        private double ProvidentFund;
        private double GrossSalary;
        private double NetSalary;

        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public double BasicSalary { get; set; }
        public int DepartmentId { get; set; }

        public Employee(int eid,string ename,string des,double bsalary,int did)
        {
            EmployeeId = eid;
            EmployeeName = ename;
            Designation = des;
            BasicSalary = bsalary;
            DepartmentId = did;
            Hra = 0.08 * bsalary;
            ProvidentFund = 0.12 * bsalary;
            GrossSalary = bsalary + Hra + 2500;
            NetSalary = GrossSalary - (2500 + ProvidentFund);
        }
        public void display()
        { 
            Console.WriteLine("1.Employee Id :"+EmployeeId);
            Console.WriteLine("2.Employee Name :"+EmployeeName);
            Console.WriteLine("3.Designation :"+Designation);
            Console.WriteLine("4.Basic Salary :"+BasicSalary);
            Console.WriteLine("5.HRA :"+Hra);
            Console.WriteLine("6.DA :"+Da);
            Console.WriteLine("7.Provident Fund :"+ProvidentFund);
            Console.WriteLine("8.Gross Salary :"+GrossSalary);
            Console.WriteLine("9.Net Salary :"+NetSalary);
            Console.WriteLine("10.Department Id :"+DepartmentId);
        }
    }
}
