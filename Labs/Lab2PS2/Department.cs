﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2PS2
{
    class Department
    {
        private int DepartmentId;
        private string DepartmentName;
        private string Location;

        public Department(int id, string dname, string location)
        {
            DepartmentId = id;
            DepartmentName = dname;
            Location = location;
        }
        public void display()
        {
            Console.WriteLine("1.Department Id :" + DepartmentId);
            Console.WriteLine("2.Department Name :" + DepartmentName);
            Console.WriteLine("3.Locaation :" + Location);
        }
    }
}
