﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6PS2
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager m = new Manager(1,"nitin","manager",10000,1);
            President p = new President(2, "john", "president", 10000, 2,50);
            Console.WriteLine("Enter your choice");
            Console.WriteLine("1. Manager Details");
            Console.WriteLine("2. President Details");
            int choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Console.WriteLine(m.ToString());
                    break;
                case 2:
                    Console.WriteLine(p.ToString());
                    break;
                default:
                    break;
            }
            m.ToString();
            p.ToString();
        }
    }
}
