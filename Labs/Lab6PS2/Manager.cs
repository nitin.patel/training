﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6PS2
{
    class Manager:Employee
    {
        private double PetrolAllowance;
        private double FoodAllowance;
        private double OtherAllowance;
        public Manager(int eid,string ename,string des,double salary,int did) : base(eid,ename,des,salary,did)
        {
            base.CalculateSalary(des);
            PetrolAllowance = 0.08 * salary;
            FoodAllowance = 0.13 * salary;
            OtherAllowance = 0.03 * salary;
        }
        public override string ToString()
        {
            string result;
            result = base.ToString() + "\nPetrol Allowance = " + PetrolAllowance + "\nFood Allowance = " + FoodAllowance + "\nOther Allowance = " + OtherAllowance;
            return result;
        }
    }
}
