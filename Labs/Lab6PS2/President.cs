﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6PS2
{
    class President:Employee
    {
        private float KilometerTravelled;
        private float TourAllowance;
        private int TelephoneAllowance;

        public President(int eid, string ename, string des, double salary, int did,float kilo) : base(eid, ename, des, salary, did)
        {
            base.CalculateSalary(des);
            TelephoneAllowance = 2000;
            KilometerTravelled = kilo;
            TourAllowance = kilo * 8;
        }

        public override string ToString()
        {
            String result;
            result = base.ToString() + "\nTelephone Allowance = 2000 \nKilometer Travelled = " + KilometerTravelled + "\nTour Allowance = " + TourAllowance;
            return result;
        }
    }
}
