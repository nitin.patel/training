﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD
{
    class Department
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string Location { get; set; }

        public void SetId(string name)
        {
            if(name == "Sales")
            {
                DepartmentId = 1;
            }
            if(name == "HR")
            {
                DepartmentId = 2;
            }
            if(name == "Production")
            {
                DepartmentId = 3;
            }
        }
        public void Display()
        {
            Console.WriteLine("1.Department Id :" + DepartmentId);
            Console.WriteLine("2.Department Name :" + DepartmentName);
            Console.WriteLine("3.Location :" + Location);
            
        }

        public void Update()
        {
            Console.WriteLine("Enter the field you want to update:");
            Console.WriteLine("1.Department Name\n2.Location");
            int choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Console.WriteLine("Current Name: {0}", DepartmentName);
                    Console.WriteLine("Enter the the updated Name:");
                    DepartmentName = Console.ReadLine();
                    Console.WriteLine("Updated Name: {0}", DepartmentName);
                    break;
                case 2:
                    Console.WriteLine("Current Location: {0}", Location);
                    Console.WriteLine("Enter the the updated Location: ");
                    Location = Console.ReadLine();
                    Console.WriteLine("Updated Location: {0}", Location);
                    break;
                default:
                    break;
            }
        }
    }
}
