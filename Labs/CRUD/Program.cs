﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD
{
    class Program
    {
        static void Main(string[] args)
        {
            string str;
            Employee e = new Employee();
            Department d = new Department();
            List<Employee> emp = new List<Employee>();
            List<Department> dep = new List<Department>();
            while (true)
            {
                Console.WriteLine("Enter your choice :");
                Console.WriteLine("1. Create a Employee");
                Console.WriteLine("2. Display Employee details based on EmployeeId");
                Console.WriteLine("3. Display all Employee Details");
                Console.WriteLine("4. Update the details of the Employee based on EmployeeId");
                Console.WriteLine("5. Delete an Employee");
                Console.WriteLine("6. Create a Department");
                Console.WriteLine("7. Display Department details based on DepartmentId");
                Console.WriteLine("8. Display all Department detials");
                Console.WriteLine("9. Update the details of the Department based on DepartmentId");
                Console.WriteLine("10. Delete a Department");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Console.WriteLine("Enter the details of the Employee :");
                        e.EmployeeId = Employee.GenerateId();
                        Console.WriteLine("Name :");
                        e.EmployeeName = Console.ReadLine();
                        Console.WriteLine("Designation :");
                        e.Designation = Console.ReadLine();
                        Console.WriteLine("Basic Salary :");
                        e.BasicSalary = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("DepartmentId(1-3) :");
                        e.DepartmentName = Console.ReadLine();
                        e.CalculateSalary(e.Designation);
                        emp.Add(e);
                        Console.WriteLine("Enter 'stop' to stop the Application:");
                        if (Console.ReadLine() == "stop")
                        {
                            break;
                        }
                        break;
                    case 2:
                        Console.WriteLine("Enter the EmployeeId of the employee whose details you want :");
                        int input = Convert.ToInt32(Console.ReadLine());
                        foreach (var employee in emp)
                        {
                            if(employee.EmployeeId == input)
                            {
                                employee.Display();
                                break;
                            }
                        }
                        Console.WriteLine("No Employee with the EmployeeId entered");
                        Console.WriteLine("Enter 'stop' to stop the Application:");
                        if (Console.ReadLine() == "stop")
                        {
                            break;
                        }
                        break;
                    case 3:
                        if (emp.Count == 0)
                        {
                            Console.WriteLine("No employee entries");
                        }
                        for (int i = 0; i < emp.Count; i++)
                        {
                            Console.WriteLine("Employee" + i + 1 + "Details :");
                            emp[i].Display();
                        }
                        Console.WriteLine("Enter 'stop' to stop the Application:");
                        if (Console.ReadLine() == "stop")
                        {
                            break;
                        }
                        break;
                    case 4:
                        Console.WriteLine("Enter the EmployeeId of the employee whose details you want to update :");
                        input = Convert.ToInt32(Console.ReadLine());
                        foreach (var employee in emp)
                        {
                            if (employee.EmployeeId == input)
                            {
                                employee.Update();
                                break;
                            }
                        }
                        Console.WriteLine("No Employee with the EmployeeId entered");
                        Console.WriteLine("Enter 'stop' to stop the Application:");
                        if (Console.ReadLine() == "stop")
                        {
                            break;
                        }
                        break;
                    case 5:
                        Console.WriteLine("Enter the EmployeeId of the employee whose details you want to delete :");
                        input = Convert.ToInt32(Console.ReadLine());
                        foreach (var employee in emp)
                        {
                            if (employee.EmployeeId == input)
                            {
                                emp.Remove(employee);
                                break;
                            }
                        }
                        Console.WriteLine("No Employee with the EmployeeId entered");
                        Console.WriteLine("Enter 'stop' to stop the Application:");
                        if (Console.ReadLine() == "stop")
                        {
                            break;
                        }
                        break;
                    case 6:
                        Console.WriteLine("Enter the Department Name");
                        d.DepartmentName = Console.ReadLine();
                        d.SetId(d.DepartmentName);
                        Console.WriteLine("Enter the Location");
                        d.Location = Console.ReadLine();
                        dep.Add(d);
                        Console.WriteLine("Enter 'stop' to stop the Application:");
                        if (Console.ReadLine() == "stop")
                        {
                            break;
                        }
                        break;
                    case 7:
                        Console.WriteLine("Enter the DepartmentId of the Department whose details you want :");
                        input = Convert.ToInt32(Console.ReadLine());
                        foreach (var department in dep)
                        {
                            if (department.DepartmentId == input)
                            {
                                department.Display();
                                break;
                            }
                        }
                        Console.WriteLine("No Department with the DepartmentId entered");
                        Console.WriteLine("Enter 'stop' to stop the Application:");
                        if (Console.ReadLine() == "stop")
                        {
                            break;
                        }
                        break;
                    case 8:
                        if (dep.Count == 0)
                        {
                            Console.WriteLine("No department entries");
                        }
                        for (int i = 0; i < dep.Count; i++)
                        {
                            Console.WriteLine("Department" + i + 1 + "Details :");
                            dep[i].Display();
                        }
                        Console.WriteLine("Enter 'stop' to stop the Application:");
                        if (Console.ReadLine() == "stop")
                        {
                            break;
                        }
                        break;
                    case 9:
                        Console.WriteLine("Enter the DepartmentId of the department whose details you want to update :");
                        input = Convert.ToInt32(Console.ReadLine());
                        foreach (var department in dep)
                        {
                            if (department.DepartmentId == input)
                            {
                                department.Update();
                                break;
                            }
                        }
                        Console.WriteLine("No Department with the DepartmentId entered");
                        Console.WriteLine("Enter 'stop' to stop the Application:");
                        if (Console.ReadLine() == "stop")
                        {
                            break;
                        }
                        break;
                    case 10:
                        Console.WriteLine("Enter the DepartmentName of the department whose details you want to delete :");
                        str = Console.ReadLine();
                        foreach (var department in dep)
                        {
                            if (department.DepartmentName == str)
                            {
                                dep.Remove(department);
                                foreach (var employee in emp)
                                {
                                    if(employee.DepartmentName == str)
                                    {
                                        emp.Remove(employee);
                                    }
                                }
                                break;
                            }
                        }
                        Console.WriteLine("No Department with the DepartmentId entered");
                        Console.WriteLine("Enter 'stop' to stop the Application:");
                        if (Console.ReadLine() == "stop")
                        {
                            break;
                        }
                        break;
                    default:
                        break;
                }

            }
        }
    }
}
