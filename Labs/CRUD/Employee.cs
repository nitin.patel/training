﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD
{
    class Employee
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public double BasicSalary { get; set; }
        public string DepartmentName { get; set; }
        private double Hra;
        private double Da;
        private double ProvidentFund;
        private double GrossSalary;
        private double NetSalary;
        private static int count = 0;

        public static int GenerateId()
        {
            count += 1;
            return count;
        }
        public void CalculateSalary(String desm)
        {
            if (desm == "Employee")
            {
                BasicSalary = BasicSalary * 1;
            }
            if (desm == "Manager")
            {
                BasicSalary = BasicSalary * 2;
            }
            if (desm == "President")
            {
                BasicSalary = BasicSalary * 3;
            }
            Hra = 0.08 * BasicSalary;
            ProvidentFund = 0.12 * BasicSalary;
            GrossSalary = BasicSalary + Hra + 2500;
            NetSalary = GrossSalary - (2500 + ProvidentFund);
        }
        public void Display()
        {
            Console.WriteLine("1.Employee Id :" + EmployeeId);
            Console.WriteLine("2.Employee Name :" + EmployeeName);
            Console.WriteLine("3.Designation :" + Designation);
            Console.WriteLine("4.Basic Salary :" + BasicSalary);
            Console.WriteLine("5.HRA :" + Hra);
            Console.WriteLine("6.DA :" + Da);
            Console.WriteLine("7.Provident Fund :" + ProvidentFund);
            Console.WriteLine("8.Gross Salary :" + GrossSalary);
            Console.WriteLine("9.Net Salary :" + NetSalary);
            Console.WriteLine("10.Department Name :" + DepartmentName);
        }
        public void Update()
        {
            Console.WriteLine("Enter the field you want to update:");
            Console.WriteLine("1.Employee Name\n2.Employee Designation\n3.Employee Salary\n4.Employee's Department Id");
            int choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Console.WriteLine("Current Name: {0}",EmployeeName);
                    Console.WriteLine("Enter the the updated Name:");
                    EmployeeName = Console.ReadLine();
                    Console.WriteLine("Updated Name: {0}",EmployeeName);
                    break;
                case 2:
                    Console.WriteLine("Current Designation: {0}",Designation);
                    Console.WriteLine("Enter the the updated Designation: ");
                    Designation = Console.ReadLine();
                    Console.WriteLine("Updated Designation: {0}", Designation);
                    break;
                case 3:
                    Console.WriteLine("Current Salary : {0}",BasicSalary);
                    Console.WriteLine("Enter the updated Salary: ");
                    BasicSalary = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Updated Salary : {0}",BasicSalary);
                    break;
                case 4:
                    Console.WriteLine("Current Department Id: {0}",DepartmentName);
                    Console.WriteLine("Enter the updated Department Id:");
                    DepartmentName = Console.ReadLine();
                    Console.WriteLine("Updated Department Id: {0}",DepartmentName);
                    break;
                default:
                    break;
            }
        }
    }
}
