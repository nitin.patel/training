﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Department d1 = new Department(1000, "HR", "Pune");
            Department d2 = new Department(2000, "Sales", "Pune");
            Employee e1 = new Employee(10001, "Ram", "Employee", 10000, 1000);
            Employee e2 = new Employee(10002, "Shyam", "Manager", 10000, 2000);

            Console.WriteLine("Enter your choice :");
            Console.WriteLine("1. Employee details");
            Console.WriteLine("2. Department details");
            int choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    Console.WriteLine("Employee 1 Details:");
                    e1.CalculateSalary(e1.Designation);
                    e1.display();
                    Console.WriteLine("\n\n\nEmployee 2 Details:");
                    e2.CalculateSalary(e2.Designation);
                    e2.display();
                    break;
                case 2:
                    Console.WriteLine("Department 1 Details:");
                    d1.display();
                    Console.WriteLine("Department 2 Details:");
                    d2.display();
                    break;
                default:
                    break;
            }
        }
    }
}
