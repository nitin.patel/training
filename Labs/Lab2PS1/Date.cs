﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2PS1
{
    class Date
    {
        public int mDay { get; set; }
        public int mMonth { get; set; }
        public int mYear { get; set; }
        public Date()
        {
            mDay = 12;
            mMonth = 12;
            mYear = 2012;
        }
        public Date(int day, int month, int year)
        {
            mDay = day;
            mMonth = month;
            mYear = year;
        }
        public void display()
        {
            Console.WriteLine(mDay + "-" + mMonth + "-" + mYear);
        }
    }
}
