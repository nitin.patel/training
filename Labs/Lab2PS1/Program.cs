﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2PS1
{
    class Program
    {
        static void Main(string[] args)
        {
            Date d1 = new Date();
            Date d2 = new Date(11, 12, 2013);
            d1.display();
            d2.display();
        }
    }
}
