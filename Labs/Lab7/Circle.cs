﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    class Circle:Shape
    {
        public override void Area()
        {
            Console.WriteLine("Enter the radius");
            double radius = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Area of Circle is :" + 3.14 * radius * radius);
        }
    }
}
