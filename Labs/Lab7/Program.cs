﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    class Program
    {
        static void Main(string[] args)
        {
            Circle c = new Circle();
            Rectangle r = new Rectangle();

            r.Area();
            c.Area();

            Shape s = new Rectangle();
            Console.WriteLine("Area of Rectangle using Base Class Reference");
            s.Area();
            s = new Circle();
            Console.WriteLine("Area of Circle using Base Class Reference");
            s.Area();

        }
    }
}
