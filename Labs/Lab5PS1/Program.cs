﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5PS1
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee e = new Employee();
            List<Employee> emp = new List<Employee>();
            bool stop = true;
            while (stop)
            {
                Console.WriteLine("Enter your choice :");
                Console.WriteLine("1. Add a new Employee and Insert its details");
                Console.WriteLine("2. Display all Employee details");
                Console.WriteLine("3. Output of ToString Method");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Console.WriteLine("Enter the details of the Employee :");
                        e.EmployeeId = Employee.GenerateId();
                        Console.WriteLine("Name :");
                        e.EmployeeName = Console.ReadLine();
                        Console.WriteLine("Designation :");
                        e.Designation = Console.ReadLine();
                        Console.WriteLine("Basic Salary :");
                        e.BasicSalary = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("DepartmentId :");
                        e.DepartmentId = Convert.ToInt32(Console.ReadLine());
                        e.CalculateSalary(e.Designation);
                        emp.Add(e);
                        Console.WriteLine("Enter 'N' to stop :");
                        if (Console.ReadLine() == "N")
                        {
                            stop=false;
                        }
                        break;
                    case 2:
                        if (emp.Count == 0)
                        {
                            Console.WriteLine("No employee entries");
                        }
                        for (int i = 0; i < emp.Count; i++)
                        {
                            Console.WriteLine("Employee" + i + 1 + "Details using ToString() :");
                            emp[i].display();
                        }
                        Console.WriteLine("Enter 'N' to stop :");
                        if (Console.ReadLine() == "N")
                        {
                            stop = false;
                        }
                        break;

                    case 3:
                        for (int i = 0; i < emp.Count; i++)
                        {
                            Console.WriteLine(emp[i].ToString());
                        }
                        if (Console.ReadLine() == "N")
                        {
                            stop = false;
                        }
                        break;
                    default:
                        break;
                }

            }
        }
    }
}
