﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //Type t = typeof(int);
            //Console.WriteLine("IsAbstract: {0}",t.IsAbstract);
            //Console.WriteLine("IsClass: {0}",t.IsClass);
            //Console.WriteLine("IsEnum: {0}",t.IsEnum);
            //Console.WriteLine("IsPrimitive: {0}",t.IsPrimitive);
            //Console.WriteLine("IsValuetype :{0}",t.IsValueType);
            //Console.WriteLine("IsStruct: {0}", true);
            //--------------------------------------------------------------------------------
            //Console.WriteLine("Please enter full path of dll to be loaded: ");
            //string asm = Console.ReadLine();

            ////load the assembly of given path
            //Assembly assembly = Assembly.LoadFile(@asm);

            ////get all the types 
            //Type[] types = assembly.GetTypes();

            ////enumarate through this collection
            //foreach (Type t in types)
            //{
            //    Console.WriteLine("TypeName : {0}",t.Name);
            //    Console.WriteLine("Is Class?? : {0}",t.IsClass);
            //    Console.WriteLine();

            //    if (t.IsClass)
            //    {
            //        Console.WriteLine("Methods in this class :");
            //        foreach (MethodInfo mi in t.GetMethods())
            //        {
            //            Console.WriteLine("Name: {0}",mi.Name);
            //        }
            //    }
            //}
            //--------------------------------------------------------------------------------

            //load the assembly
            Assembly assembly = Assembly.LoadFile(@"C:\training\ReflectionApp\TripstackLib\bin\Debug\tripstacklib.dll");
            Console.WriteLine("1. Addition");
            Console.WriteLine("2. Subtract");
            Console.WriteLine("3. Square");
            Console.WriteLine("4. Cube");
            Console.WriteLine("Enter operation number:");

            int choice = Convert.ToInt32(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    //load class
                    Type t = assembly.GetType("TripstackLib.ComplexMath");
                    //get default constructor
                    ConstructorInfo constructor = t.GetConstructor(Type.EmptyTypes);
                    //construct an object
                    object instance = constructor.Invoke(null);
                    //get method to be invoked
                    MethodInfo method = t.GetMethod("Add");

                    Console.WriteLine("Enter first number :");
                    int x = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter second number :");
                    int y = Convert.ToInt32(Console.ReadLine());
                    
                    //arrange parameters to be passed to method
                    object[] parameters = { x, y };

                    //invoke method
                    object result = method.Invoke( instance,parameters);
                    //use method's return data
                    Console.WriteLine("Addition of {0} and {1} is : {2}",x,y,result);
                    break;
                case 2:
                     t = assembly.GetType("TripstackLib.ComplexMath");
                     constructor = t.GetConstructor(Type.EmptyTypes);
                     instance = constructor.Invoke(null);
                     method = t.GetMethod("Subtract");

                    Console.WriteLine("Enter first number :");
                    x = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter second number :");
                    y = Convert.ToInt32(Console.ReadLine());
                    object[] subparameters = { x, y };

                     result = method.Invoke(instance, subparameters);
                    Console.WriteLine("Subtraction of {0} and {1} is : {2}", x, y, result);
                    break;
                case 3:
                    t = assembly.GetType("TripstackLib.SimpleMath");
                    constructor = t.GetConstructor(Type.EmptyTypes);
                    instance = constructor.Invoke(null);
                    method = t.GetMethod("Square");

                    Console.WriteLine("Enter  number :");
                    x = Convert.ToInt32(Console.ReadLine());
                    
                    object[] sparameters = { x};

                    result = method.Invoke(instance, sparameters);
                    Console.WriteLine("Square of {0}  is : {1}", x, result);

                    break;
                case 4:
                    t = assembly.GetType("TripstackLib.SimpleMath");
                    constructor = t.GetConstructor(Type.EmptyTypes);
                    instance = constructor.Invoke(null);
                    method = t.GetMethod("Cube");

                    Console.WriteLine("Enter  number :");
                    x = Convert.ToInt32(Console.ReadLine());

                    object[] cparameters = { x };

                    result = method.Invoke(instance, cparameters);
                    Console.WriteLine("Square of {0}  is : {1}", x, result);

                    break;
                default:
                    break;
            }


        }
    }
}
