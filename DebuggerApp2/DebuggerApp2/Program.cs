﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DebuggerApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Account account1 = new Account(50.00M);

            Console.WriteLine("account1 balance {0:c}", account1.Balance);

            decimal withdrawalAmount;
            Console.WriteLine("Enter withdrawal amount for account1 :");
            withdrawalAmount = Convert.ToDecimal(Console.ReadLine());

            Console.WriteLine("\nWithdrawal {0:c} from account1 balance.......", withdrawalAmount);

            account1.Debit(withdrawalAmount);

            Console.WriteLine("account1 balance is :{0:c}", account1.Balance);
            Console.WriteLine();

            Console.WriteLine("Enter credit amount for account1:");
            decimal creditAmount = Convert.ToDecimal(Console.ReadLine());

            Console.WriteLine("Crediting {0:c} to account1 balance", creditAmount);
            account1.Credit(creditAmount);

            Console.WriteLine("account1 balance is :{0:c}", account1.Balance);
            Console.WriteLine();

            Console.ReadLine();
        }
    }
}
