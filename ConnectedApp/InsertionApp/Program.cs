﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace InsertionApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter following values- ");
            Console.WriteLine("Name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Description: ");
            string des = Console.ReadLine();
            Console.WriteLine("Category: ");
            string cat = Console.ReadLine();
            Console.WriteLine("Price: ");
            double price = Convert.ToDouble(Console.ReadLine());

            string insert = string.Format("insert into Products " +
                "(Name ,Description,Category,Price) values ('{0}', '{1}', '{2}', '{3}')", name, des, cat, price);
            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False"))
            {
                conn.Open();
                SqlCommand command = new SqlCommand();
                command.CommandText = insert;
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;

                var result = command.ExecuteNonQuery();
                if (result > 0)
                {
                    Console.WriteLine("Product Inserted Successfully");
                }
                else
                {
                    Console.WriteLine("Product not inserted");
                }
                conn.Close();
            }
        }
    }
}
