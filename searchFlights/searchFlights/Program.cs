﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace searchFlights
{
    class Program
    {
        static void Main(string[] args)
        {
            string origin = args[1];
            string destination = args[3];
            List<string> data = new List<string>();
            StreamReader reader = new StreamReader(@"C:\training\searchFlights\Provider1.txt");
            while (!reader.EndOfStream)
            {
                data.Add(reader.ReadLine());
            }
            reader.Close();


            reader = new StreamReader(@"C:\training\searchFlights\Provider2.txt");
            while (!reader.EndOfStream)
            {
                string tmp = reader.ReadLine();
                tmp = tmp.Replace('-', '/');
                data.Add(tmp);
            }
            reader.Close();


            reader = new StreamReader(@"C:\training\searchFlights\Provider3.txt");
            while (!reader.EndOfStream)
            {
                string tmp = reader.ReadLine();
                tmp = tmp.Replace('|', ',');
                data.Add(tmp);
            }
            reader.Close();

            data = data.Distinct().ToList();  //33 unique entries
            List<string> result = new List<string>();
            foreach (string flight in data)
            {
                string[] individual = new string[5];
                individual = flight.Split(',');
                if (origin == individual[0] && destination == individual[2])
                { 
                    result.Add(flight);
                }
            }

            foreach (string flight in result)
            {
                string[] individual = new string[5];
                individual = flight.Split(',');
                Console.WriteLine(individual[0] + " --> " + individual[2] + "\t(" + individual[1] + " --> " + individual[3] + ")  -  " + individual[4]);
            }
        }        
    }
}
