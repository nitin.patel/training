﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFApp
{
    class Program
    {
        static void Main(string[] args)
        {
            TrainingDBEntities db = new TrainingDBEntities();
            //Product prd = new Product { Name_ = "Tennis", Description = "tennis", Category = "tennis", Price = 100 };
            //db.Products.Add(prd);
            
            var prd = db.Products.Where(p => p.Id == 11).SingleOrDefault();
            //prd.Price += 100;
            db.Products.Remove(prd);
            db.SaveChanges();
        }
    }
}
